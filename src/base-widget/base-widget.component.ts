import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public persons: Array<any> = [
    { "name" : "Donald Trump",
      "img":"../assets/img/noimg.png",
      "statement" : 1718191}, 
    {"name" : "Barack Obama",
      "img":"../assets/img/noimg.png",
      "statement" : 7881},
    {"name" : "Trump",
      "img":"../assets/img/noimg.png",
      "statement" : 5869},
    {"name" : "Najib Razak",
      "img":"../assets/img/noimg.png",
      "statement" : 5286},
    {"name" : "Theresa May",
      "img":"../assets/img/noimg.png",
      "statement" : 5217},
    {"name" : "King Jong Nam",
      "img":"../assets/img/noimg.png",
      "statement" : 4878}];

}
